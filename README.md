# Repo Exporter

This is a simple tool to export all repositories from a GitLab group to a local directory. It preserves the directory structure of the group and the repositories. Also a summary file is created with the repository names, Avatars, and URLs.

## Usage

```bash
python main.py lujobi-projects <THE-TOKEN>
```

Then you can find the exported repositories in the `exports` directory. this can be zipped as follows 
    
```bash
zip -r exports.zip exports
```
