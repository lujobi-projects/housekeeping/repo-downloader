import os
import sys
import requests
from tqdm import tqdm
from os import getenv
import json
from exporter_types import (
    GitlabGroupInfo,
    GitlabProjectInfo,
    Summary,
    EnhancedJSONEncoder,
)

TOKEN = getenv("GITLAB_PRIVATE_TOKEN", None)
GITLAB_GROUP_ID = getenv("GITLAB_GROUP_ID", None)

print(sys.argv)

if not TOKEN:
    TOKEN = sys.argv[2]
    GITLAB_GROUP_ID = sys.argv[1]


if TOKEN is None or GITLAB_GROUP_ID is None:
    print("error no token or group id")
    exit(1)


def getall(url):
    all_res = []
    req = requests.get(f"{url}?page=1", headers={"PRIVATE-TOKEN": TOKEN})
    res = req.json()
    page = 2
    while res != [] and req.status_code == 200:
        all_res.extend(res)
        req = requests.get(f"{url}?page={page}", headers={"PRIVATE-TOKEN": TOKEN})
        res = req.json()
        page += 1
    all_res.extend(res)
    return all_res


groups = getall(f"https://gitlab.com/api/v4/groups/{GITLAB_GROUP_ID}/descendant_groups")

all_groups = list(
    map(
        lambda x: GitlabGroupInfo(
            x["id"], x["full_path"], x["description"], x["avatar_url"], x["name"]
        ),
        groups,
    )
)


all_projects: list[GitlabProjectInfo] = []

for g in tqdm(all_groups):
    projects = getall(f"https://gitlab.com/api/v4/groups/{g.id}/projects")
    project_infos = list(
        map(
            lambda x: GitlabProjectInfo(
                x["id"],
                x["path_with_namespace"],
                x["ssh_url_to_repo"],
                x["description"],
                x["avatar_url"],
                x["name"],
            ),
            projects,
        )
    )
    os.makedirs(g.path, exist_ok=True)

    for p in project_infos:
        os.makedirs(p.path, exist_ok=True)
        res = os.system(f"git clone {p.ssh_id} {p.path}")
        res = os.system(
            f"git -C {p.path} branch -r | grep -v '\\->' | "
            'sed "s,\\x1B\\[[0-9;]*[a-zA-Z],,g" | '
            "while read remote; "
            f'do git -C {p.path}  branch --track "${{remote#origin/}}" "$remote" || true; '
            "done",
        )
        if res != 0:
            print(f"error cloning {p.path}")
            continue
        if os.path.isfile(f"{p.path}/.gitattributes"):
            os.system(f"git -C {p.path} lfs fetch --all")

        os.makedirs(f"avatars-{p.path}", exist_ok=True)
        os.system(
            f'curl --header "PRIVATE-TOKEN: {TOKEN}" {p.avatar_url} > avatars-{p.path}/avatar.png'
        )

    all_projects.extend(project_infos)


summary = Summary()

for g in tqdm(all_groups):
    ns = None
    for p in g.path.split("/"):
        if ns is None:
            ns = summary
        if p not in ns.subgroups:
            ns.subgroups[p] = Summary()
        ns = ns.subgroups[p]
    ns.id = g.id
    ns.path = g.path
    ns.description = g.description
    ns.avatar_url = g.avatar_url
    ns.name = g.name


for g in tqdm(all_projects):
    ns: Summary = None
    splitted = g.path.split("/")
    for p in splitted[0:-1]:
        if ns is None:
            ns = summary
        if p not in ns.subgroups:
            ns.subgroups[p] = Summary()
        ns = ns.subgroups[p]
    ns.projects[splitted[-1]] = GitlabProjectInfo(
        g.id,
        g.path,
        g.ssh_id,
        g.description,
        g.avatar_url,
        g.name,
    )

os.makedirs("exports", exist_ok=True)
os.system(f"mv avatars-{GITLAB_GROUP_ID} exports")
os.system(f"mv {GITLAB_GROUP_ID} exports")

with open("exports/summary.json", "w") as f:
    json.dump(summary.subgroups[GITLAB_GROUP_ID], f, cls=EnhancedJSONEncoder)
