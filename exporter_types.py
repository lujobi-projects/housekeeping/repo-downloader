from dataclasses import dataclass, field

import dataclasses, json


class EnhancedJSONEncoder(json.JSONEncoder):
    def default(self, o):
        if dataclasses.is_dataclass(o):
            return dataclasses.asdict(o)
        return super().default(o)


@dataclass
class GitlabGroupInfo:
    id: str
    path: str
    description: str
    avatar_url: str
    name: str


@dataclass
class GitlabProjectInfo:
    id: str
    path: str
    ssh_id: str
    description: str
    avatar_url: str
    name: str


@dataclass
class Summary:
    id: str = "id"
    path: str = "path"
    description: str = "description"
    avatar_url: str = "avatar_url"
    name: str = "name"
    projects: dict[str, GitlabProjectInfo] = field(default_factory=dict)
    subgroups: dict[str, GitlabGroupInfo] = field(default_factory=dict)
